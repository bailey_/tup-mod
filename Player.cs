﻿using System;
using MelonLoader;
using TimeLoop;
using TLObjectIdentity;
using UnityEngine;

namespace modrw {
    public static class Player {
        private static readonly Func<string> _infoSus = () => 
            "\nLocal suspected: " + HackReporter.IsSuspectedHacker(TcNetworkOculusPlayer.localPlayer.PhotonPlayer);

        private static readonly Func<string> _infoRoom = () =>
            "\nCurrent room: " + TcNetworkOculusPlayer.LocalPlayerRoomNetworked.roomName;
        
        private static readonly Func<string> _infoCount = () =>
            "\nRemote players count: " + TcNetworkOculusPlayer.remotePlayers.Count;
        

        public static void Info() {
            MelonModLogger.Log(
                _infoSus() +
                _infoRoom() +
                _infoCount()
                );
        }

        public static void Spawn(int _id) {
            if (!Enum.IsDefined(typeof(U16NetObjType), Convert.ToUInt16(_id))) return;
            
            ObjNetIdentManager.GuaranteedSpawnNewLocalObject((U16NetObjType)_id,
                TcNetworkOculusPlayer.localPlayer.leftHand.transform.position,
                Quaternion.Euler(0, 0, 0));
            
            MelonModLogger.Log("Spawned " + (U16NetObjType)_id);
        }

        public static void SetMask(int _id) {
            TcNetworkOculusPlayer.SetLocalPlayerMaskDesign((TcPlayerMask.MaskDesign)_id, true);
            MelonModLogger.Log("Mask changed to " + (TcPlayerMask.MaskDesign)_id);
        }
    }
}