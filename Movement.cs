﻿using UnityEngine;
using MelonLoader;

namespace modrw {
    public static class Movement {
        
        public static void Ascend() {
            Transform _transform = TcNetworkOculusPlayer.localPlayer.transform;
            Vector3 _position = _transform.position;
            TcNetworkOculusPlayer.localPlayer.TeleportLocal(new Vector3(_position.x, _position.y + 10, _position.z), _transform.rotation);
            MelonModLogger.Log("y += 10");
        }
            
        public static void Descend() {
            Transform _transform = TcNetworkOculusPlayer.localPlayer.transform;
            Vector3 position = _transform.position;
            TcNetworkOculusPlayer.localPlayer.TeleportLocal(new Vector3(position.x, position.y - 5, position.z), _transform.rotation);
            MelonModLogger.Log("y -= 5");
        }
        
        public static void Teleport(int pid) {
            if (TcNetworkOculusPlayer.remotePlayers[pid] != null) {
                TcNetworkOculusPlayer.localPlayer.TeleportLocal(TcNetworkOculusPlayer.remotePlayers[pid].transform.position, TcNetworkOculusPlayer.localPlayer.transform.rotation);
                MelonModLogger.Log("Teleported to " + TcNetworkOculusPlayer.remotePlayers[pid].OculusUserName);
            }
        }

        public static void tpWorkshop() {
            TcNetworkOculusPlayer.localPlayer.TeleportLocal(new Vector3(-34, -552, -19), TcNetworkOculusPlayer.localPlayer.transform.rotation);
        }
    }
}