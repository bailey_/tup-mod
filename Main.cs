﻿using System.Threading;
using MelonLoader;

namespace modrw {
    public class TestMod : MelonMod {
        public override void OnApplicationStart() {
            new Thread(ConsoleChecks.Main).Start();
        }
    }
}