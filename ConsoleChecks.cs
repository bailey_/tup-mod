﻿using UnityEngine;

namespace modrw {
    public static class ConsoleChecks {
        public static void Main() {
            while (true) {
                string s = System.Console.ReadLine();
                
                if (s == null) return;
                
                string[] _s = s.Split();

                switch (_s[0]) {
                    case "info":
                        Player.Info();
                        break;
                    case "tpws": // Remove this in future.
                        Movement.tpWorkshop();
                        break;
                    case "a":
                        Movement.Ascend();
                        break;
                    case "d":
                        Movement.Descend();
                        break;
                    case "sp":
                        Player.Spawn(int.Parse(_s[1]));
                        break;
                    case "ms":
                        Player.SetMask(int.Parse(_s[1]));
                        break;
                    case "tp":
                        Movement.Teleport(int.Parse(_s[1]));
                        break;
                }
            }
        }
    }
}