# Please, do not use this mod to troll and ruin someone else's game. Thanks.

### Usage
* info - get player and server info.
* sp ITEM_ID - spawn item by id. The spawned item does not appear immediately. It must be picked up in order for it to appear.
* a - ascend (Teleport up).
* d - descend (Teleport down).
* ms - set player mask.
* tp PLAYER_ID - teleport to player.
* tpws - teleport to workshop.

### WARNING
Using teleportation and changing mask will give you "Cheater" tag. Use Ghidra to patch game's GameAssembly.dll .