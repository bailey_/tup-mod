﻿using System.Resources;
using System.Reflection;
using System.Runtime.InteropServices;
using MelonLoader;

[assembly: AssemblyTitle(modrw.BuildInfo.Description)]
[assembly: AssemblyDescription(modrw.BuildInfo.Description)]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(modrw.BuildInfo.Company)]
[assembly: AssemblyProduct(modrw.BuildInfo.Name)]
[assembly: AssemblyCopyright("Created by " + modrw.BuildInfo.Author)]
[assembly: AssemblyTrademark(modrw.BuildInfo.Company)]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion(modrw.BuildInfo.Version)]
[assembly: AssemblyFileVersion(modrw.BuildInfo.Version)]
[assembly: NeutralResourcesLanguage("en")]
[assembly: MelonModInfo(typeof(modrw.TestMod), modrw.BuildInfo.Name, modrw.BuildInfo.Version, modrw.BuildInfo.Author, modrw.BuildInfo.DownloadLink)]

[assembly: MelonModGame(null, null)]